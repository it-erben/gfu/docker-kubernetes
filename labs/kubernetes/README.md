# Übungsaufgaben zu Kubernetes

- [Kubectl Basics](./00-kubectl-basics)
- [Pods und Services](./01-pods-services)
- [ReplicaSets](./02-replicaset)
- Deployments
  - [Deployments und Rolling Update](./03a-deployment-rolling-update)
  - [Deployments und Recreate Update](./03b-deployment-recreate)
- [StatefulSets und Headless Services](./04-stateful-set-headless-service)
- [Volumes](./05-volumes)
- [ConfigMaps](./06-configmaps)
- [Casestudy: Nextcloud](./07-casestudy-nextcloud)
- [Readiness Probes](./08-readiness-probe)
- [RBAC](./09-rbac)
- [Pod Security Standards](./10-pss)
- [HorizontalPodAutoscaler](./11-hpa)
- [Prometheus](./12-prometheus)
